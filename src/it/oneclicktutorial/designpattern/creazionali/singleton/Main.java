package it.oneclicktutorial.designpattern.creazionali.singleton;

public class Main {
    public static void main(String[] args) {
        //Errore di compilazione: Costruttore non visibile
        //Singleton singleton = new Singleton();

        //Metodo corretto
        Singleton singleton = Singleton.getInstance();
    }
}

