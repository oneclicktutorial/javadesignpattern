package it.oneclicktutorial.designpattern.creazionali.singleton;

public class Singleton {   //Crea un oggetto Singleton
    private static Singleton instance = new Singleton();

    // Rendiamo il costruttore privato, in questo modo
    // la classe non può essere instanziata
    private Singleton(){}

    // Ritorna l'unico oggetto creato
    public static Singleton getInstance(){
        stampa();
        return instance;
    }

    private static void stampa(){
        System.out.println("[SINGLETON]: OK!");
    }
}
