package it.oneclicktutorial.designpattern.creazionali.lazyinitialization;

public class Main {
    public static void main(String args[]) {
        String semeCarta = "Cuori";
        String valoreCarta = "Sette";
        Carta carta1;
        Carta carta2;
        Carta carta3;

        carta1 = Carta.getCarta(semeCarta,valoreCarta);
        System.out.println("[MAIN]: CARTA1::SEME = "+carta1.getSeme());
        System.out.println("[MAIN]: CARTA1::VALORE = "+carta1.getValore()+"\n");
        carta2 = Carta.getCarta(semeCarta,valoreCarta);
        System.out.println("[MAIN]: CARTA2::SEME = "+carta2.getSeme());
        System.out.println("[MAIN]: CARTA2::VALORE = "+carta2.getValore()+"\n");

        valoreCarta = "Cinque";
        carta3 = Carta.getCarta(semeCarta,valoreCarta);
        System.out.println("[MAIN]: CARTA3::SEME = "+carta3.getSeme());
        System.out.println("[MAIN]: CARTA3::VALORE = "+carta3.getValore());

    }
}
