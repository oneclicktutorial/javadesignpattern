package it.oneclicktutorial.designpattern.creazionali.lazyinitialization;

import java.util.*;

public class Carta {

    //Mappatura tra carta (String) e la sua istanza (Carta)
    private static Map<String, Carta> carte = new HashMap<String, Carta>();

    //Contiene i valori dell'istanza corrente
    private String seme;
    private String valore;

    public String getSeme() {
        return seme;
    }

    public String getValore() {
        return valore;
    }

    //Rendiamo il costruttore privato per forzare l'uso del metodo factory.
    private Carta(String seme, String valore){
        this.seme=seme;
        this.valore=valore;
        carte.put(seme, this);
        carte.put(valore, this);
    }

     //Metodo Lazy con pattern Factory: recupera un'istanza di Carta
     // se presente, altrimenti istanzia un nuovo oggetto.
     public static Carta getCarta(String seme, String valore) {
        System.out.println("[CARTA]: getCarta::start()");
        Carta carta;
        if (carte.containsKey(seme) && (carte.containsKey(valore))){
            System.out.println("[CARTA]: getCarta::Carta trovata!");
            carta = carte.get(seme); // recupera l'istanza di quel tipo
        } else {
            System.out.println("[CARTA]: getCarta::Carta non trovata! Nuovo oggetto creato.");
            carta = new Carta(seme, valore); // inizializzazione lazy (pigra)
        }
         System.out.println("[CARTA]: getCarta::end()");
        return carta;
    }
}