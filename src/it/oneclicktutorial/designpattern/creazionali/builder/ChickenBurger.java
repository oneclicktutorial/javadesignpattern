package it.oneclicktutorial.designpattern.creazionali.builder;

public class ChickenBurger extends Hamburger {

    @Override
    public float prezzo() {
        return 5.5f;
    }

    @Override
    public String nome() {
        return "Chicken Burger";
    }
}
