package it.oneclicktutorial.designpattern.creazionali.builder;

public class Contenitore implements Imballaggio {

    @Override
    public String imballa() {
        return "Contenitore";
    }
}
