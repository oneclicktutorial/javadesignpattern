package it.oneclicktutorial.designpattern.creazionali.builder;

public class PepsiCola extends BevandaFredda {

    @Override
    public float prezzo() {
        return 2.5f;
    }

    @Override
    public String nome() {
        return "Pepsi Cola";
    }
}