package it.oneclicktutorial.designpattern.creazionali.builder;

public interface Oggetto {
    public String nome();
    public float prezzo();
    public Imballaggio imballaggio();
}
