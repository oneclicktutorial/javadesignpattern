package it.oneclicktutorial.designpattern.creazionali.builder;

public class Bottiglia implements Imballaggio {

    @Override
    public String imballa() {
        return "Bottiglia";
    }
}
