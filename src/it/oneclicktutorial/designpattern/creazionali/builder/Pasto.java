package it.oneclicktutorial.designpattern.creazionali.builder;

import java.util.ArrayList;
import java.util.List;

public class Pasto {
    private List<Oggetto> oggetti = new ArrayList<Oggetto>();

    public void addOggetto(Oggetto oggetto){
        oggetti.add(oggetto);
    }

    public float getCosto(){
        float costo = 0.0f;

        for (Oggetto item : oggetti) {
            costo += item.prezzo();
        }
        return costo;
    }

    public void visualizzaOggetti(){
        System.out.println("[PASTO]: visualizzaOggetti()::start");
        for (Oggetto oggetto : oggetti) {
            System.out.print("[PASTO]: Oggetto -> " + oggetto.nome());
            System.out.print(", [PASTO]: Imballaggio -> " + oggetto.imballaggio().imballa());
            System.out.println(", [PASTO]: Prezzo -> " + oggetto.prezzo());
        }
        System.out.println("[PASTO]: visualizzaOggetti()::end");
    }
}
