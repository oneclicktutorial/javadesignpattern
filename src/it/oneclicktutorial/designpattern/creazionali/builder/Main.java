package it.oneclicktutorial.designpattern.creazionali.builder;

public class Main {
    public static void main(String[] args) {

        PastoBuilder pastoBuilder = new PastoBuilder();

        Pasto pastoVegano = pastoBuilder.preparaPastoVegano();
        System.out.println("[MAIN]: ### PASTO VEGANO ###");
        pastoVegano.visualizzaOggetti();
        System.out.println("[MAIN]: Costo Totale: " + pastoVegano.getCosto());

        Pasto pastoNonVegano = pastoBuilder.preparaPastoNonVegano();
        System.out.println("\n[MAIN]: ### PASTO NON VEGANO ###");
        pastoNonVegano.visualizzaOggetti();
        System.out.println("[MAIN]: Costo Totale: " + pastoNonVegano.getCosto());
    }
}