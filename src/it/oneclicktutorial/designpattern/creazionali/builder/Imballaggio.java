package it.oneclicktutorial.designpattern.creazionali.builder;

public interface Imballaggio {
    public String imballa();
}
