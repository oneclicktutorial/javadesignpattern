package it.oneclicktutorial.designpattern.creazionali.builder;

public class PastoBuilder {

    public Pasto preparaPastoVegano (){
        System.out.println("[PASTOBUILDER]: preparaPastoVegano()::start");
        Pasto pasto = new Pasto();
        pasto.addOggetto(new HamburgerVegano());
        pasto.addOggetto(new CocaCola());
        System.out.println("[PASTOBUILDER]: preparaPastoVegano()::end");
        return pasto;
    }

    public Pasto preparaPastoNonVegano (){
        System.out.println("[PASTOBUILDER]: preparaPastoNonVegano()::start");
        Pasto pasto = new Pasto();
        pasto.addOggetto(new ChickenBurger());
        pasto.addOggetto(new PepsiCola());
        System.out.println("[PASTOBUILDER]: preparaPastoNonVegano()::end");
        return pasto;
    }
}
