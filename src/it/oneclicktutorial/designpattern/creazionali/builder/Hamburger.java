package it.oneclicktutorial.designpattern.creazionali.builder;

public abstract class Hamburger implements Oggetto {

    @Override
    public Imballaggio imballaggio() {
        return new Contenitore() {
        };
    }

    @Override
    public abstract float prezzo();
}