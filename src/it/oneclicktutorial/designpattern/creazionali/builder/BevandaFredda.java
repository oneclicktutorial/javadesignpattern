package it.oneclicktutorial.designpattern.creazionali.builder;

public abstract class BevandaFredda implements Oggetto {

    @Override
    public Imballaggio imballaggio() {
        return new Bottiglia();
    }

    @Override
    public abstract float prezzo();
}
