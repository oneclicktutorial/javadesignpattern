package it.oneclicktutorial.designpattern.creazionali.builder;

public class CocaCola extends BevandaFredda {

    @Override
    public float prezzo() {
        return 3.0f;
    }

    @Override
    public String nome() {
        return "Coca Cola";
    }
}