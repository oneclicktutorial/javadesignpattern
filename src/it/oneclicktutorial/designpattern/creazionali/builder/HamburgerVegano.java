package it.oneclicktutorial.designpattern.creazionali.builder;

public class HamburgerVegano extends Hamburger {

    @Override
    public float prezzo() {
        return 8.0f;
    }

    @Override
    public String nome() {
        return "Hamburger Vegano";
    }
}
