package it.oneclicktutorial.designpattern.creazionali.abstractfactory;

public class GenericFactory extends AbstractFactory {
    @Override
    Figura getFigura(String figura) {
        return new Cerchio();
    }
}
