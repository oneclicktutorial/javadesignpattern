package it.oneclicktutorial.designpattern.creazionali.abstractfactory;

public abstract class AbstractFactory {
    abstract Figura getFigura(String figura) ;
}
