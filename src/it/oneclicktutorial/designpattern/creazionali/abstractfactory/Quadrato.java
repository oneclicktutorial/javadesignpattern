package it.oneclicktutorial.designpattern.creazionali.abstractfactory;

public class Quadrato implements Figura {
    private final static int base = 2;
    private static int area = 0;

    @Override
    public void disegna() {
        System.out.println("[QUADRATO] : disegna()");
    }

    @Override
    public void calcolaArea() {
        System.out.println("[QUADRATO] : calcolaArea()::start");
        area = base*base;
        System.out.println("[QUADRATO] : calcolaArea()::end");
    }

    public void getArea() {
        System.out.println("[QUADRATO: area = " + area + " ]");
    }
}
