package it.oneclicktutorial.designpattern.creazionali.abstractfactory;

public class Main {
    public static void main(String[] args) {
        AbstractFactory factory1 = FactoryProducer.getFactory(true);
        Figura figura1 = factory1.getFigura("RETTANGOLO");
        figura1.disegna();
        figura1.calcolaArea();
        figura1.getArea();

        Figura figura2 = factory1.getFigura("QUADRATO");
        figura2.disegna();
        figura2.calcolaArea();
        figura2.getArea();

        AbstractFactory factory2 = FactoryProducer.getFactory(false);
        Figura figura3 = factory2.getFigura("CERCHIO");
        figura3.disegna();
        figura3.calcolaArea();
        figura3.getArea();

    }
}
