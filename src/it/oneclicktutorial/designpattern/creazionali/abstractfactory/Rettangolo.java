package it.oneclicktutorial.designpattern.creazionali.abstractfactory;

public class Rettangolo implements Figura {

    private final static int base = 2;
    private final static int altezza = 5;
    private static int area = 0;

    @Override
    public void disegna() {
        System.out.println("[RETTANGOLO] : disegna()");
    }

    @Override
    public void calcolaArea() {
        System.out.println("[RETTANGOLO] : calcolaArea()::start");
        area = base * altezza;
        System.out.println("[RETTANGOLO] : calcolaArea()::end");
    }

    public void getArea() {
        System.out.println("[RETTANGOLO: area = " + area + " ]");
    }
}
