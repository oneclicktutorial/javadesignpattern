package it.oneclicktutorial.designpattern.creazionali.abstractfactory;

public interface Figura {
    void disegna();
    void calcolaArea();
    void getArea();
}
