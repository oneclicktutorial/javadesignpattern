package it.oneclicktutorial.designpattern.creazionali.abstractfactory;

public class FiguraFactory extends AbstractFactory {
    @Override
    public Figura getFigura(String figura) {
        if("RETTANGOLO".equalsIgnoreCase(figura)) {
            return new Rettangolo();
        } else if("QUADRATO".equalsIgnoreCase(figura)) {
            return new Quadrato();
        } else {
            return null;
        }
    }
}
