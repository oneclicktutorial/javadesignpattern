package it.oneclicktutorial.designpattern.comportamentali.nullobject;

public class Main {
    public static void main(String[] args) {

        Cliente cliente1 = ClienteFactory.getNome("Roberto");
        Cliente cliente2 = ClienteFactory.getNome("Giovanni");
        Cliente cliente3 = ClienteFactory.getNome("Giulia");
        Cliente cliente4 = ClienteFactory.getNome("Antonio");

        System.out.println("[MAIN]: Lista clienti");
        System.out.println(cliente1.getNome());
        System.out.println(cliente2.getNome());
        System.out.println(cliente3.getNome());
        System.out.println(cliente4.getNome());
    }
}
