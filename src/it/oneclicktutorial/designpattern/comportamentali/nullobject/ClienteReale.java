package it.oneclicktutorial.designpattern.comportamentali.nullobject;

public class ClienteReale extends Cliente {

    public ClienteReale(String nome) {
        this.nome = nome;
    }

    @Override
    public String getNome() {
        System.out.println("[CLIENTEREALE]: getNome()::start");
        System.out.println("[CLIENTEREALE]: getNome()::end");
        return "[CLIENTEREALE]: "+nome;
    }

    @Override
    public boolean isNull() {
        return false;
    }
}