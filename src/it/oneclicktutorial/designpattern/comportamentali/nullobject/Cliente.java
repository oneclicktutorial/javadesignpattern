package it.oneclicktutorial.designpattern.comportamentali.nullobject;

public abstract class Cliente {
    protected String nome;
    public abstract boolean isNull();
    public abstract String getNome();
}
