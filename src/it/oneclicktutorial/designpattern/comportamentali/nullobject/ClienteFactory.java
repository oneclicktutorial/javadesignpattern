package it.oneclicktutorial.designpattern.comportamentali.nullobject;

public class ClienteFactory {

    public static final String[] nomi = {"Roberto", "Giovanni", "Giulia"};

    public static Cliente getNome(String name){
        for (int i = 0; i < nomi.length; i++) {
            if (nomi[i].equalsIgnoreCase(name)){
                return new ClienteReale(name);
            }
        }
        return new ClienteNullo();
    }
}