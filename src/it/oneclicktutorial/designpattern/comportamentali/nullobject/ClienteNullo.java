package it.oneclicktutorial.designpattern.comportamentali.nullobject;

public class ClienteNullo extends Cliente {

    @Override
    public String getNome() {
        System.out.println("[CLIENTENULLO]: getNome()::start");
        System.out.println("[CLIENTENULLO]: getNome()::end");
        return "[CLIENTENULLO]: Cliente non trovato!";
    }

    @Override
    public boolean isNull() {
        return true;
    }
}
