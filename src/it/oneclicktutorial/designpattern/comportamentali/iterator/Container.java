package it.oneclicktutorial.designpattern.comportamentali.iterator;

public interface Container {
    public Iterator getIterator();
}