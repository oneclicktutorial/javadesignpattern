package it.oneclicktutorial.designpattern.comportamentali.iterator;

public class Main {

    public static void main(String[] args) {
        Repository repository = new Repository();

        System.out.println("[MAIN]: Recupero nomi in Repository"+"\n");
        for(Iterator iterator = repository.getIterator(); iterator.hasNext();){
            String name = (String)iterator.next();
            System.out.println("[MAIN]: Nome -> " + name);
        }
    }
}
