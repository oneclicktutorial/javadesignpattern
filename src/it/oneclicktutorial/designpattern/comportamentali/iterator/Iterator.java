package it.oneclicktutorial.designpattern.comportamentali.iterator;

public interface Iterator {
    public boolean hasNext();
    public Object next();
}