package it.oneclicktutorial.designpattern.comportamentali.iterator;

public class Repository implements Container {
    public String nomi[] = {"Roberto" , "Giovanni" ,"Giulia" , "Andrea"};

    @Override
    public Iterator getIterator() {
        return new NomiIterator();
    }

    private class NomiIterator implements Iterator {
        int index;

        @Override
        public boolean hasNext() {
            if(index < nomi.length){
                System.out.println("[REPOSITORY]: hasNext()::true");
                return true;
            }
            System.out.println("[REPOSITORY]: hasNext()::false");
            return false;
        }

        @Override
        public Object next() {
            if(this.hasNext()){
                System.out.println("[REPOSITORY]: next()::"+nomi[index++]);
                index--;
                return nomi[index++];
            }
            return null;
        }
    }
}
