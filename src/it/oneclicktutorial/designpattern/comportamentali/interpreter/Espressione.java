package it.oneclicktutorial.designpattern.comportamentali.interpreter;

public interface Espressione {
    public boolean interpreta(String contesto);
}