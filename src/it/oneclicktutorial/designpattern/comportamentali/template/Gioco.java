package it.oneclicktutorial.designpattern.comportamentali.template;

public abstract class Gioco {
    abstract void init();
    abstract void start();
    abstract void end();

    //template method
    public final void gioca(){

        //Inizializza il gioco
        init();

        //Lancia il gioco
        start();

        //Termina il gioco
        end();
    }
}