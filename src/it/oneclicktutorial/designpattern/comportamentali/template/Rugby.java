package it.oneclicktutorial.designpattern.comportamentali.template;

public class Rugby extends Gioco {
    @Override
    void end() {
        System.out.println("[RUGBY]::end()");
    }

    @Override
    void init() {
        System.out.println("[RUGBY]::init()");
    }

    @Override
    void start() {
        System.out.println("[RUGBY]::start()");
    }
}