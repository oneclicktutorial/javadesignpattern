package it.oneclicktutorial.designpattern.comportamentali.template;

public class Main {
    public static void main(String[] args) {
        Gioco gioco = new Calcio();
        gioco.gioca();
        gioco = new Rugby();
        gioco.gioca();
    }
}