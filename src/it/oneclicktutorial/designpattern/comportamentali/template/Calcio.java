package it.oneclicktutorial.designpattern.comportamentali.template;

public class Calcio extends Gioco {
    @Override
    void end() {
        System.out.println("[CALCIO]::end()");
    }

    @Override
    void init() {
        System.out.println("[CALCIO]::init()");
    }

    @Override
    void start() {
        System.out.println("[CALCIO]::start()");
    }
}