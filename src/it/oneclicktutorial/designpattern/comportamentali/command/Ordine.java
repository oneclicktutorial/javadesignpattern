package it.oneclicktutorial.designpattern.comportamentali.command;

public interface Ordine {
    void esegui();
}