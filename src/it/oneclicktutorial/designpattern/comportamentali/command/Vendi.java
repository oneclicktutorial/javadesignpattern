package it.oneclicktutorial.designpattern.comportamentali.command;

public class Vendi implements Ordine {
    private Magazzino magazzino;

    public Vendi(Magazzino magazzino){
        this.magazzino = magazzino;
    }

    public void esegui() {
        magazzino.vendi();
    }
}
