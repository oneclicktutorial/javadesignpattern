package it.oneclicktutorial.designpattern.comportamentali.command;

public class Magazzino {

    private String nome = "Prodotto_1";
    private int quantità = 25;

    public void compra(){
        System.out.println("[MAGAZZINO]: [ NOME: "+nome+", QUANTITA': " + quantità +" ] COMPRATO");
    }
    public void vendi(){
        System.out.println("[MAGAZZINO]: [ NOME: "+nome+", QUANTITA': " + quantità +" ] VENDUTO");
    }
}
