package it.oneclicktutorial.designpattern.comportamentali.command;

public class Main {
    public static void main(String[] args) {
        Magazzino magazzino = new Magazzino();

        Compra compraOrdine = new Compra(magazzino);
        Vendi vendiOrdine = new Vendi(magazzino);

        Command broker = new Command();
        broker.prendiOrdine(compraOrdine);
        broker.prendiOrdine(vendiOrdine);

        broker.piazzaOrdine();
    }
}
