package it.oneclicktutorial.designpattern.comportamentali.command;

import java.util.ArrayList;
import java.util.List;

public class Command {
    private List<Ordine> ordineList = new ArrayList<Ordine>();

    public void prendiOrdine(Ordine order){
        System.out.println("[COMMAND]: prendiOrdine()::start");
        ordineList.add(order);
        System.out.println("[COMMAND]: prendiOrdine()::Ordine aggiunto alla lista!");
        System.out.println("[COMMAND]: prendiOrdine()::end");
    }

    public void piazzaOrdine(){
        System.out.println("[COMMAND]: piazzaOrdine()::start");
        for (Ordine ordine : ordineList) {
            ordine.esegui();
        }
        System.out.println("[COMMAND]: piazzaOrdine()::Ordini eseguiti!");
        ordineList.clear();
        System.out.println("[COMMAND]: piazzaOrdine()::end");
    }
}