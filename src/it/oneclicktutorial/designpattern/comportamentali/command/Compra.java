package it.oneclicktutorial.designpattern.comportamentali.command;

public class Compra implements Ordine {
    private Magazzino magazzino;

    public Compra(Magazzino magazzino){
        this.magazzino = magazzino;
    }

    public void esegui() {
        magazzino.compra();
    }
}