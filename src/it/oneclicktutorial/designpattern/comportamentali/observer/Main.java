package it.oneclicktutorial.designpattern.comportamentali.observer;

public class Main {
    public static void main(String[] args) {
        Soggetto soggetto = new Soggetto();

        new OsservatoreEsadecimale(soggetto);
        new OsservatoreOttale(soggetto);
        new OsservatoreBinario(soggetto);

        System.out.println("[MAIN]: Cambio stato -> 15");
        soggetto.setStato(15);
        System.out.println("[MAIN]: Cambio stato -> 10");
        soggetto.setStato(10);
    }
}
