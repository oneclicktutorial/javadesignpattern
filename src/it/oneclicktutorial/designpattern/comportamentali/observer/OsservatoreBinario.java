package it.oneclicktutorial.designpattern.comportamentali.observer;

public class OsservatoreBinario extends Observer {

    public OsservatoreBinario(Soggetto soggetto){
        this.soggetto = soggetto;
        this.soggetto.attach(this);
    }

    @Override
    public void aggiorna() {
        System.out.println("[OSSERVATORE_BINARIO]: aggiorna()::start");
        System.out.println("Binary String: " + Integer.toBinaryString( soggetto.getStato()));
    }
}