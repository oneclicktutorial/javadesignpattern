package it.oneclicktutorial.designpattern.comportamentali.observer;

public class OsservatoreEsadecimale extends Observer {

    public OsservatoreEsadecimale(Soggetto soggetto){
        this.soggetto = soggetto;
        this.soggetto.attach(this);
    }

    @Override
    public void aggiorna() {
        System.out.println("[OSSERVATORE_ESADECIMALE]: aggiorna()::start");
        System.out.println("[OSSERVATORE_ESADECIMALE]: Stringa esadecimale: " + Integer.toHexString(soggetto.getStato()).toUpperCase());
        System.out.println("[OSSERVATORE_ESADECIMALE]: aggiorna()::end");
    }
}