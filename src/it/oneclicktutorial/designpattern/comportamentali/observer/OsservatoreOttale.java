package it.oneclicktutorial.designpattern.comportamentali.observer;

public class OsservatoreOttale extends Observer {

    public OsservatoreOttale(Soggetto soggetto){
        this.soggetto = soggetto;
        this.soggetto.attach(this);
    }

    @Override
    public void aggiorna() {
        System.out.println("[OSSERVATORE_OTTALE]: aggiorna()::start");
        System.out.println("[OSSERVATORE_OTTALE]: Stringa Ottale: " + Integer.toOctalString( soggetto.getStato()));
        System.out.println("[OSSERVATORE_OTTALE]: aggiorna()::end");
    }
}