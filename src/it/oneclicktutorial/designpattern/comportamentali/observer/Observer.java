package it.oneclicktutorial.designpattern.comportamentali.observer;

public abstract class Observer {
    protected Soggetto soggetto;
    public abstract void aggiorna();
}