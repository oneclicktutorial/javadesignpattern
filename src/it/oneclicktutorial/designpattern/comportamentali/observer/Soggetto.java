package it.oneclicktutorial.designpattern.comportamentali.observer;

import java.util.ArrayList;
import java.util.List;

public class Soggetto {

    private List<Observer> observers = new ArrayList<Observer>();
    private int stato;

    public int getStato() {
        return stato;
    }

    public void setStato(int stato) {
        System.out.println("[SOGGETTO]: setStato()::start");
        this.stato = stato;
        notifica();
        System.out.println("[SOGGETTO]: setStato()::end");
    }

    public void attach(Observer observer){
        observers.add(observer);
    }

    public void notifica(){
        System.out.println("[SOGGETTO]: notifica()::start");
        for (Observer observer : observers) {
            observer.aggiorna();
        }
        System.out.println("[SOGGETTO]: notifica()::end");
    }
}