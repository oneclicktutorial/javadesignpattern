package it.oneclicktutorial.designpattern.strutturali.decorator;

public class Cerchio implements Figura {

    @Override
    public void disegna() {
        System.out.println("[CERCHIO]: disegna()");
    }
}
