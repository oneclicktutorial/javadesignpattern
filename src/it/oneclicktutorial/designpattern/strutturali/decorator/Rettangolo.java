package it.oneclicktutorial.designpattern.strutturali.decorator;

public class Rettangolo implements Figura {

    @Override
    public void disegna() {
        System.out.println("[RETTANGOLO]: disegna()");
    }
}