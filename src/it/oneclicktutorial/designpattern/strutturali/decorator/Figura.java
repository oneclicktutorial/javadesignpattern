package it.oneclicktutorial.designpattern.strutturali.decorator;

public interface Figura {
    void disegna();
}
