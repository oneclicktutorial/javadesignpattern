package it.oneclicktutorial.designpattern.strutturali.decorator;

public class Main {
    public static void main(String[] args) {

        Figura circle = new Cerchio();
        Figura redCircle = new FiguraRossaDecorator(new Cerchio());
        Figura redRectangle = new FiguraRossaDecorator(new Rettangolo());
        System.out.println("[MAIN]: Cerchio con bordi normali");
        circle.disegna();
        System.out.println("\n[MAIN]: Cerchio con bordi rossi");
        redCircle.disegna();
        System.out.println("\n[MAIN]: Rettangolo con bordi rossi");
        redRectangle.disegna();
    }
}
