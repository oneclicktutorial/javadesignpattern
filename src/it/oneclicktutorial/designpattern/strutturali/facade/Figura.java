package it.oneclicktutorial.designpattern.strutturali.facade;

public interface Figura {
    void disegna();
}
