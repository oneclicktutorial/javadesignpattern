package it.oneclicktutorial.designpattern.strutturali.facade;

public class Facade {
    private Figura cerchio;
    private Figura rettangolo;
    private Figura quadrato;

    public Facade() {
        cerchio = new Cerchio();
        rettangolo = new Rettangolo();
        quadrato = new Quadrato();
    }

    public void disegnaCerchio(){
        System.out.println("[FACADE]: disegnaCerchio()");
        cerchio.disegna();
    }
    public void disegnaRettangolo(){
        System.out.println("[FACADE]: disegnaRettangolo()");
        rettangolo.disegna();
    }
    public void disegnaQuadrato(){
        System.out.println("[FACADE]: disegnaQuadrato()");
        quadrato.disegna();
    }
}
