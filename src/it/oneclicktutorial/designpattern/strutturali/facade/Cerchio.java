package it.oneclicktutorial.designpattern.strutturali.facade;

public class Cerchio implements Figura {

    @Override
    public void disegna() {
        System.out.println("[CERCHIO]: disegna()");
    }
}
