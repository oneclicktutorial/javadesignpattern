package it.oneclicktutorial.designpattern.strutturali.facade;

public class Quadrato implements Figura {

    @Override
    public void disegna() {
        System.out.println("[QUADRATO]: disegna()");
    }
}
