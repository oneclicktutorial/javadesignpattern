package it.oneclicktutorial.designpattern.strutturali.proxy;

public class ImmagineProxy implements Immagine{

    private ImmagineReale immagineReale;
    private String nomeFile;

    public ImmagineProxy(String nomeFile){
        this.nomeFile = nomeFile;
    }

    @Override
    public void mostra() {
        System.out.println("[IMMAGINEPROXY]: mostra()::start");
        if(immagineReale == null){
            System.out.println("[IMMAGINEPROXY]: mostra()::immagine null");
            immagineReale = new ImmagineReale(nomeFile);
        }
        immagineReale.mostra();
        System.out.println("[IMMAGINEPROXY]: mostra()::end");
    }
}