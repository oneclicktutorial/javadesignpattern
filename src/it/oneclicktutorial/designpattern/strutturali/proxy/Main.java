package it.oneclicktutorial.designpattern.strutturali.proxy;

public class Main {
    public static void main(String[] args) {
        Immagine immagine = new ImmagineProxy("IMG_001.jpg");

        immagine.mostra();
        System.out.println("");
        immagine.mostra();
    }
}