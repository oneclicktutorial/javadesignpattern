package it.oneclicktutorial.designpattern.strutturali.proxy;

public interface Immagine {
    void mostra();
}
