package it.oneclicktutorial.designpattern.strutturali.proxy;

public class ImmagineReale implements Immagine {

    private String nomeFile;

    public ImmagineReale(String nomeFile){
        this.nomeFile = nomeFile;
        caricaImmagine(nomeFile);
    }

    @Override
    public void mostra() {
        System.out.println("[IMMAGINEREALE]: mostra(): " + nomeFile);
    }

    private void caricaImmagine(String nomeFile){
        System.out.println("[IMMAGINEREALE]: caricaImmagine(): " + nomeFile);
    }
}
