package it.oneclicktutorial.designpattern.strutturali.bridge;

public class Cerchio extends Figura {
    private int x, y, raggio;

    public Cerchio(int x, int y, int radius, DisegnaAPI disegnaAPI) {
        super(disegnaAPI);
        this.x = x;
        this.y = y;
        this.raggio = radius;
    }

    public void disegna() {
        disegnaAPI.disegnaCerchio(raggio,x,y);
    }
}
