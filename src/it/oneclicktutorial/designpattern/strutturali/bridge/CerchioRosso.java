package it.oneclicktutorial.designpattern.strutturali.bridge;

public class CerchioRosso implements DisegnaAPI {
    @Override
    public void disegnaCerchio(int raggio, int x, int y) {
        System.out.println("[CERCHIOROSSO]: Cerchio disegnato! ");
        System.out.println("[CERCHIOROSSO]: COLORE: ROSSO, RAGGIO: " + raggio + ", X: " + x + ", Y:" + y + "]");
    }
}
