package it.oneclicktutorial.designpattern.strutturali.bridge;

public class CerchioVerde implements DisegnaAPI {
    @Override
    public void disegnaCerchio(int raggio, int x, int y) {
        System.out.println("[CERCHIOVERDE]: Cerchio disegnato! ");
        System.out.println("[CERCHIOVERDE]: COLORE: VERDE, RAGGIO: " + raggio + ", X: " + x + ", Y:" + y + "]");
    }
}
