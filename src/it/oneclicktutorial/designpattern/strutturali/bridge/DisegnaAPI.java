package it.oneclicktutorial.designpattern.strutturali.bridge;

public interface DisegnaAPI {
    public void disegnaCerchio(int raggio, int x, int y);
}
