package it.oneclicktutorial.designpattern.strutturali.adapter;

public class AdattatoreMultimediale implements LettoreMultimediale{

    LettoreMultimedialeAvanzato lettoreMultimedialeAvanzato;

    public AdattatoreMultimediale(String tipoFile){
        if(tipoFile.equalsIgnoreCase("vlc") ){
            lettoreMultimedialeAvanzato = new LettoreVlc();
        }else if (tipoFile.equalsIgnoreCase("mp4")){
            lettoreMultimedialeAvanzato = new LettoreMp4();
        }
    }

    @Override
    public void esegui(String tipoFile, String nomeFile) {
        if(tipoFile.equalsIgnoreCase("vlc")){
            lettoreMultimedialeAvanzato.eseguiVlc(nomeFile);
        }
        else if(tipoFile.equalsIgnoreCase("mp4")){
            lettoreMultimedialeAvanzato.eseguiMp4(nomeFile);
        }
    }
}

