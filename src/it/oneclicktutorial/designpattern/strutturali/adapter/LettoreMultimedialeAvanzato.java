package it.oneclicktutorial.designpattern.strutturali.adapter;

public interface LettoreMultimedialeAvanzato {
    public void eseguiVlc(String nomeFile);
    public void eseguiMp4(String nomeFile);
}
