package it.oneclicktutorial.designpattern.strutturali.adapter;

public class LettoreVlc implements LettoreMultimedialeAvanzato{
    @Override
    public void eseguiVlc(String nomeFile) {
        System.out.println("[LETTOREVLC] : Riproduzione file Vlc:  "+ nomeFile);
    }

    @Override
    public void eseguiMp4(String nomeFile) {
    }
}
