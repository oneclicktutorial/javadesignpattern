package it.oneclicktutorial.designpattern.strutturali.adapter;

public class LettoreMp4 implements LettoreMultimedialeAvanzato {
    @Override
    public void eseguiVlc(String nomeFile) {
    }

    @Override
    public void eseguiMp4(String nomeFile) {
        System.out.println("[LETTOREVLC] : Riproduzione file mp4:  "+ nomeFile);
    }
}

