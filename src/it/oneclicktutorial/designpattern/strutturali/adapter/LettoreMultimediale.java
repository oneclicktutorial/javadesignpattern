package it.oneclicktutorial.designpattern.strutturali.adapter;

public interface LettoreMultimediale {
    public void esegui(String tipoFile, String nomeFile);
}
