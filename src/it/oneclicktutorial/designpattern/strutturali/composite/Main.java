package it.oneclicktutorial.designpattern.strutturali.composite;

public class Main {
    public static void main(String[] args) {
        Utility utility = new Utility();
        Impiegato CEO = new Impiegato("Gennaro","CEO", 50000);
        Impiegato responsabileVendite = new Impiegato("Roberto","Responsabile Vendite", 35000);
        Impiegato responsabileMarketing = new Impiegato("Michele","Responsabile Marketing", 35000);
        Impiegato impMark1 = new Impiegato("Laura","Marketing", 24000);
        Impiegato impMark2 = new Impiegato("Bob","Marketing", 24000);
        Impiegato impVen1 = new Impiegato("Riccardo","Vendite", 24000);
        Impiegato impVen2 = new Impiegato("Annalisa","Vendite", 24000);

        utility.componiAzienda(CEO, responsabileVendite, responsabileMarketing, impVen1, impVen2, impMark1, impMark2);
        utility.stampaAzienda(CEO);
    }
}