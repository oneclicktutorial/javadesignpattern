package it.oneclicktutorial.designpattern.strutturali.composite;

public class Utility {
    private static final String toLogPrefix = "[UTILITY]: ";
    private String toLogMethod;
    private String toLog;

    public void componiAzienda(Impiegato CEO, Impiegato responsabileVendite, Impiegato responsabileMarketing,
                               Impiegato impVen1, Impiegato impVen2, Impiegato impMark1, Impiegato impMark2) {
        CEO.aggiungiSubordinato(responsabileVendite);
        CEO.aggiungiSubordinato(responsabileMarketing);
        responsabileVendite.aggiungiSubordinato(impVen1);
        responsabileVendite.aggiungiSubordinato(impVen2);
        responsabileMarketing.aggiungiSubordinato(impMark1);
        responsabileMarketing.aggiungiSubordinato(impMark2);
        toLogMethod = "componiAzienda()::";
        toLog = "Azienda creata con successo!";
        log(toLogPrefix, toLogMethod, toLog);
    }
    public void stampaAzienda(Impiegato CEO) {
        toLogMethod = "stampaAzienda()::";
        toLog = "Riepilogo Azienda:";
        log(toLogPrefix, toLogMethod, toLog);
        System.out.println("\n"+CEO);
        for (Impiegato responsabili : CEO.getSubordinati()) {
            System.out.println(responsabili);
            for (Impiegato employee : responsabili.getSubordinati()) {
                System.out.println(employee);
            }
        }
    }

    public void log(String toLogPrefix, String toLogMethod, String toLog) {
        System.out.println(toLogPrefix + toLogMethod + toLog);
    }
}
