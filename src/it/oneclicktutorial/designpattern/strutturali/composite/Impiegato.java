package it.oneclicktutorial.designpattern.strutturali.composite;

import java.util.ArrayList;
import java.util.List;

public class Impiegato {
    private String nome;
    private String dipartimento;
    private int salario;
    private List<Impiegato> subordinati;
    private static final String toLogPrefix = "[IMPIEGATO]: ";
    private String toLogMethod;
    private String toLog;
    Utility utility = new Utility();

    public Impiegato(String nome,String dipartimento, int salario) {
        this.nome = nome;
        this.dipartimento = dipartimento;
        this.salario = salario;
        subordinati = new ArrayList<Impiegato>();
    }

    public void aggiungiSubordinato(Impiegato impiegato) {
        subordinati.add(impiegato);
        toLogMethod = "aggiungiSubordinato()::";
        toLog = "Subordinato inserito con successo!";
        utility.log(toLogPrefix, toLogMethod, toLog);
    }

    public void rimuoviSubordinato(Impiegato impiegato) {
        subordinati.remove(impiegato);
        toLogMethod = "rimuoviSubordinato()::";
        toLog = "Subordinato rimosso con successo!";
        utility.log(toLogPrefix, toLogMethod, toLog);
    }

    public List<Impiegato> getSubordinati(){
        return subordinati;
    }

    public String toString(){
        return ("[IMPIEGATO] :[ Nome : " + nome + ", dipartimento : " + dipartimento + ", salario :" + salario+" ]");
    }
}
